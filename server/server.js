const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');

const { v4: uuidv4 } = require('uuid');
const app = express();
const products = require('./db');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

http.createServer(app).listen(3001, () => {
  console.log('Listen on 0.0.0.0:3001');
});

app.get('/', (_, res) => {
  res.send({ status: 200 });
});

app.get('/products', (req, res) => {
  const query = req.query;

  const { category, minPrice, maxPrice, page: pageParameter } = query;

  const perPage = 24;
  const page = pageParameter || 1;

  let filteredProducts = products;
  if (category || minPrice || maxPrice) {
    filteredProducts = products.filter((product) => {
      // Filter by minimum price
      if (minPrice && minPrice > product.price) {
        return false;
      }

      // Filter by maximum price
      if (maxPrice && maxPrice < product.price) {
        return false;
      }

      // Filter by category, case insensitive
      if (
        category &&
        category.toLowerCase() !== product.category.toLowerCase()
      ) {
        return false;
      }
      return true;
    });
  }

  // Paginate
  const result = filteredProducts.slice(
    perPage * page - perPage,
    perPage * page
  );

  res.send(result);
});
app.get('/products/:id/similar', (req, res) => {
  const productId = req.params.id;
  const limit = 10;

  const product = products.find((product) => product.id === productId);

  if (!product) {
    res.status(404).send('Product not found');
  }

  const price = product.price;
  const category = product.category;

  // Filter by category and remove product that was passed in
  const filteredProducts = products.filter((prod) => {
    return (
      prod.id !== productId &&
      prod.category.toLowerCase() === category.toLowerCase()
    );
  });

  // Middle-out sorting, to get the products with nearest price
  filteredProducts.sort(
    (a, b) =>
      Math.abs(a.price - price) - Math.abs(b.price - price) || b.price - a.price
  );

  res.send(filteredProducts.slice(0, limit));
});

app.post('/products/create', (req, res) => {
  const id = uuidv4();
  const name = req.body.name;
  const category = req.body.category;
  const price = req.body.price;

  const newProduct = {
    id,
    name,
    category,
    price,
  };

  products.push(newProduct);

  res.send(newProduct);
});

process.on('SIGINT', function () {
  process.exit();
});
