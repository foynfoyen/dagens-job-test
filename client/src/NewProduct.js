import React, { useState } from 'react';
import axios from 'axios';

const NewProduct = () => {
  const [name, setName] = useState('');
  const [category, setCategory] = useState('Meat');
  const [price, setPrice] = useState(0);

  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    setSuccess(false);
    setError(false);

    return axios
      .post('http://0.0.0.0:3001/products/create', {
        name,
        category,
        price,
      })
      .then(function (response) {
        setSuccess(true);
        setName('');
        setCategory('Meat');
        setPrice(0);
      })
      .catch(function (error) {
        setError(true);
      });
  };

  return (
    <>
      <h2>Create new product</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
        </div>
        <div className="form-group">
          <label htmlFor="category">Category</label>
          <select
            id="category"
            name="category"
            onChange={(e) => setCategory(e.target.value)}
            value={category}
          >
            <option>Meat</option>
            <option>Fish</option>
            <option>Greens</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="price">Price</label>
          <input
            type="number"
            id="price"
            name="price"
            onChange={(e) => setPrice(e.target.value)}
            value={price}
          />
        </div>
        <div className="form-group">
          <button type="submit">Create</button>
        </div>
      </form>
      {error && <div>Something went wrong!</div>}
      {success && <div>Product created!</div>}
    </>
  );
};

export default NewProduct;
