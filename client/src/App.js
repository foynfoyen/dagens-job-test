import NewProduct from './NewProduct';

const App = () => (
  <div className="app">
    <NewProduct />
  </div>
);

export default App;
